## node-signal-bot

Small suggestion: this is easier to run on Debian/Ubuntu, so that's what we recommend
if you get to choose

### Dependencies

- [signal-cli](https://github.com/AsamK/signal-cli)
- dbus

### Preparation

Get signal-cli up and running. The easiest way is probably to download a release from
https://github.com/AsamK/signal-cli/releases and extract it in your home directory.

```
wget 'https://github.com/AsamK/signal-cli/releases/download/v0.6.2/signal-cli-0.6.2.tar.gz'
tar -xvf signal-cli-0.6.2.tar.gz
```

signal-cli seems to require jre8 or higher, so make sure you have that installed. You will
also need to ensure you have the following package installed for dbus to operate:

- Debian: libunixsocket-java
- Fedora: libmatthew-java

  On Fedora, you also need to create a symbolic link

  `ln -s /usr/lib64/libmatthew-java/libunix-java.so /usr/lib64/libunix-java.so`

More information can be find in the [wiki](https://github.com/AsamK/signal-cli/wiki)

If `npm install` fails, make sure you are running an LTS version of node (v10.16.0 tested)

Before running the bot, you need to run signal-cli. It is highly recommended for security
reasons to run signal-cli and signal-bot as a separate user from the rest of the system

### Running signal-cli

In 3 simple steps:

`signal-cli -u $PHONENUMBER register`

`signal-cli -u $PHONENUMBER verify $CODE`

`signal-cli -u $PHONENUMBER daemon`

### Usage

index.js is the best resource.

Example use:

```js
const signalBot = require('signal-bot');

signalBot.init(callback);

function callback(err, success) {
    if(err) {
        console.log('failed');
        return;
    }

    // callback receives (timestamp, source/sender, groupId, message, attachments)
    // where groupId is the signal group ID
    signalBot.onMessage(console.log);

    // message, attachments, recipient
    signalBot.sendMessage(console.log, "Message", [], "+1xxxxxxxxxx")
}
```

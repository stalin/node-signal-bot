// Documentation at https://github.com/Shouqun/node-dbus
// recommends this if no X server running
process.env.DISPLAY = ':0';
//process.env.DBUS_SESSION_BUS_ADDRESS = 'unix:path=/run/dbus/system_bus_socket';

const dbus = require('dbus');
const sessionbus = dbus.getBus('session');

let interface = null;

// cb should accept (err, success)
// if err is not null, there was a problem
function init(cb) {
	sessionbus.getInterface(
		'org.asamk.Signal',
		'/org/asamk/Signal',
		'org.asamk.Signal',
		(err, _interface) => { 
			if (err) {
				return cb(err);
			}

			interface = _interface
			return cb(null, true);
		});
}

// cb is currently unused but will return result in the future
// attachments and receipients are arrays
// recipient should be of the format '+1xxxxxxxxxx'
// (or a different area code)
function sendMessage(cb, message, attachments, recipients) {
	interface.sendMessage(message, attachments, recipients);
}

//cb should accept timestamp, source, groupId, message, attachments 
function onMessage(cb) {
	interface.on('MessageReceived', cb)
}

module.exports = {init, sendMessage, onMessage}
